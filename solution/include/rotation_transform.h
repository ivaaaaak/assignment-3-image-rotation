#ifndef ROTATION_TRANSFORM_H
#define ROTATION_TRANSFORM_H

struct image rotate(struct image const source);

#endif 
