#include "image.h"
#include "bmp_format.h"
#include "rotation_transform.h"
#include <stdio.h>

#define ARGUMENT_NUM 3

int main( int argc, char** argv ) {
    if (argc < ARGUMENT_NUM) {
        fprintf(stderr, "2 arguments are required: <source-image> <transformed-image>");
    }
    else {

        FILE* in = fopen(argv[1], "rb");

        if (in != NULL) {

            struct image img = {0};
            enum read_status cur_read_status = from_bmp(in, &img);

            if (cur_read_status != READ_OK) {
                fprintf(stderr, "Failed to read from first file");

                if (fclose(in) == 0) {
                    fprintf(stderr, "Failed to close first file");
                }
                destroy_image(&img);
                return 1;
            }


            if (fclose(in) == 0) {

                struct image rotated_img = rotate(img);
                destroy_image(&img);
                FILE* out = fopen(argv[2], "wb");

                if (out != NULL) {

                    enum write_status cur_write_status = to_bmp(out, &rotated_img);

                    if (cur_write_status != WRITE_OK) {
                        fprintf(stderr, "Failed to write into second file");

                        if (fclose(out) == 0) {
                            fprintf(stderr, "Failed to close second file");
                        }
                        destroy_image(&rotated_img);
                        return 1;
                    }
                    destroy_image(&rotated_img);

                    if (fclose(out) != 0) {
                        fprintf(stderr, "Failed to close second file");
                        return 1;
                    }
                }
                else {
                    fprintf(stderr, "Failed to open second file");
                    destroy_image(&rotated_img);
                    return 1;
                }
            }
            else {
                fprintf(stderr, "Failed to close first file");
                destroy_image(&img);
                return 1;
            }
        }
        else {
            fprintf(stderr, "Failed to open first file");
            return 1;
        }
    }
    return 0;
}
